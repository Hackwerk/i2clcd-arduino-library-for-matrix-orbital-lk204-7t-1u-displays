/*	Copyright 2014 Martijn Stommels
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or 
 *	any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */ 

#ifndef I2CLCD_h
#define I2CLCD_h

#include "Arduino.h"
#include <Wire.h>


#define I2CLCD_KEYPAD_LEFT_TOP 65
#define I2CLCD_KEYPAD_LEFT_BOTTOM 71
#define I2CLCD_KEYPAD_UP 66
#define I2CLCD_KEYPAD_LEFT 68
#define I2CLCD_KEYPAD_RIGHT 67
#define I2CLCD_KEYPAD_DOWN 72
#define I2CLCD_KEYPAD_MIDDLE 69

class I2CLCD
{
  public:
    I2CLCD(int address);
    void print(char *text);
    void print(int code);
    void println(char *text);
    void clear();
    void autoScroll(bool enabled);
    void autoLineWrap(bool enabled);
    void setCursorPosition(int column, int row);
    void blinkingCursor(bool enabled);
    void setGPO(int GPONumber, bool state);
    
    void setKeypadBacklight(bool enabled);
    void setKeypadBacklight(int value);
    
    void setDisplayBacklight(bool enabled);
    void setDisplayBacklight(int value);
    
    void autoTransmitKeyPresses(bool enabled);
    
    byte checkForKeyPress();
    
  private:
    int _address;
};


#endif I2CLCD_h
