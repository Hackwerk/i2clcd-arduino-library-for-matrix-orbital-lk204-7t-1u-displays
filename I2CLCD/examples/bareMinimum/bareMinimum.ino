/*	Copyright 2014 Martijn Stommels
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or 
 *	any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *     
 *     ---
 *     This example contains the bare minimum code needed to use 
 *     I2CLCD Arduino library for Matrix Orbital LK204-7T-1U displays.
 */

#include <Wire.h>
#include <I2CLCD.h>

I2CLCD lcd(0x28);

void setup() {
  
  Wire.begin();
  
  
  lcd.clear();
  lcd.print("Kraftwerk!");
}

void loop() {
  
  // do stuff here
  
}
