/*	Copyright 2014 Martijn Stommels
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or 
 *	any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *     
 *     ---
 *     This example demonstrates the keypad functionality of this
 *     I2CLCD Arduino library for Matrix Orbital LK204-7T-1U displays.
 */ 
 
// tell Arduino to use the libary T(you have to include Wire.h first!)
#include <Wire.h>
#include <I2CLCD.h>

// Init a new LCD object at I2C address 0x28
I2CLCD lcd(0x28);

void setup() {
  // join I2C bus
  Wire.begin();
  
  // clear the display
  lcd.clear();
  
  // tell the user what to do
  lcd.println("Please press a key.");
  

}

void loop()
{
  // check if a key has been pressed
  int key = lcd.checkForKeyPress();
  
  if(key > 0)
  {
    // clear the display
    lcd.clear();
    
    lcd.println("You pressed: ");
    lcd.print("the ");
    
    // for each key we wil print a special message
    switch(key)
    {
      case I2CLCD_KEYPAD_LEFT_TOP :
        lcd.print("top left");
      break;
      
      case I2CLCD_KEYPAD_LEFT_BOTTOM :
        lcd.print("bottom left");
      break;
      
      case I2CLCD_KEYPAD_UP :
        lcd.print("up");
      break;
      
      case I2CLCD_KEYPAD_LEFT :
        lcd.print("left");
      break;
      
      case I2CLCD_KEYPAD_RIGHT :
        lcd.print("right");
      break;
      
      case I2CLCD_KEYPAD_DOWN :
        lcd.print("down");
      break;
      
      case I2CLCD_KEYPAD_MIDDLE :
         lcd.print("middle");
      break;
    }
    lcd.println(" button.");
    
  }
  // wait for a few milliseconds so we dont overload the display
  delay(10);
  
}
