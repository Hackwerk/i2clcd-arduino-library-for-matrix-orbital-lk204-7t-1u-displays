#######################################
# Syntax Coloring I2CLCD
#######################################

#######################################
# Datatypes (KEYWORD1)
#######################################

I2CLCD	KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################
print	KEYWORD2
println	KEYWORD2
clear	KEYWORD2
autoScroll	KEYWORD2
autoLineWrap	KEYWORD2
setCursorPosition	KEYWORD2
blinkingCursor	KEYWORD2
setGPO	KEYWORD2
setKeypadBacklight	KEYWORD2
setDisplayBacklight	KEYWORD2
autoTransmitKeyPresses	KEYWORD2
checkForKeyPress	KEYWORD2

#######################################
# Constants (LITERAL1)
#######################################
I2CLCD_KEYPAD_LEFT_TOP
I2CLCD_KEYPAD_LEFT_BOTTOM
I2CLCD_KEYPAD_UP
I2CLCD_KEYPAD_LEFT
I2CLCD_KEYPAD_RIGHT
I2CLCD_KEYPAD_DOWN
I2CLCD_KEYPAD_MIDDLE
